package principal;

import java.util.ArrayList;
import java.util.Scanner;

/*
Faça um programa que tenha uma classe chamada Fazenda. Os atributos da sua classe são:

Registro (String)
Nome (String)
Caixa (float -> dinheiro guardado na conta bancária).
Finalidade (int -> Gado de corte [1], gado leiteiro[2] ou agricultura [3]).
Número de funcionários (int).

O nosso programa deve ter um vetor de fazendas de três posições. Faça um menu que permita:

1- Inserir os dados das três fazendas.
2- Mostrar os dados das três fazendas.
3- Permitir a busca de uma fazenda a partir do registro.
4- Permitir o empréstimo de uma fazenda A para uma fazenda B.
5- Permitir o trânsito de funcionários de uma fazenda A para uma fazenda B.
6- Sair
*/

public class Principal {

    public static void main(String[] args) {
        
        Scanner entrada = new Scanner(System.in);
  
        String regis, regisA, regisB;
        int opc=0, i=0, validador=0, a=0, b=0;
        
        //Alocação dinâmica
        ArrayList <Fazenda> lista;
        lista = new ArrayList();
        
        
        //Laço para criar instanciar as fazendas
        Fazenda vetorFazendas[];
        vetorFazendas = new Fazenda[3];
        
        //Instanciando fazendas
        for(i=0; i<3; i++){
            vetorFazendas[i] = new Fazenda();
        }
        
        while(opc!=6){
            System.out.println("\n ----- MENU -----");
            System.out.print("\n 1- Inserir dados das fazendas");
            System.out.print("\n 2- Mostrar dados das fazendas");
            System.out.print("\n 3- Buscar fazenda pelo registro");
            System.out.print("\n 4- Permitir empréstimo de uma fazenda A para uma fazenda B");
            System.out.print("\n 5- Permitir o trânsito de funcionários de uma fazenda A para uma fazenda B");
            System.out.print("\n 6- Sair \n -> ");
            opc = entrada.nextInt();
            
            switch(opc){

                case 1:
                    System.out.println("\n --- Inserir dados das fazendas ---");
                    for(i=0; i<3; i++){
                        System.out.println("\n Fazenda "+(i+1));

                        System.out.print("\n Registro: ");
                        entrada.skip("\n"); // Vou colocar pra não pular
                        vetorFazendas[i].registro = entrada.nextLine();
                        System.out.print("\n Nome: ");
                        vetorFazendas[i].nome = entrada.nextLine();
                        System.out.print("\n Caixa: ");
                        vetorFazendas[i].caixa = entrada.nextFloat();
                        System.out.print("\n Finalidade: ");
                        vetorFazendas[i].finalidade = entrada.nextInt();
                        System.out.print("\n Número de funcionários: ");
                        vetorFazendas[i].numeroFuncionarios = entrada.nextInt();
                    }                
                break;

                case 2:
                    System.out.println("\n --- Mostrar dados das fazendas ---");
                    for(i=0; i<3; i++){
                        
                        System.out.println("\n Fazenda "+(i+1));
                        System.out.print("\n Registro: "+vetorFazendas[i].registro);                        
                        System.out.print("\n Nome: "+vetorFazendas[i].nome);                        
                        System.out.print("\n Caixa: "+vetorFazendas[i].caixa);                        
                        System.out.print("\n Finalidade: "+vetorFazendas[i].finalidade);                        
                        System.out.println("\n Número de funcionários: "+vetorFazendas[i].numeroFuncionarios);
                        
                    }
                break;

                case 3:
                    System.out.println("\n --- Busca a partir de um registro ---");
                    System.out.print("\n Digite o registro: ");
                    entrada.skip("\n");
                    regis = entrada.nextLine();
                    for(i=0; i<3; i++){
                        if(vetorFazendas[i].registro.equals(regis)){
                            System.out.println("\n Fazenda "+(i+1));

                            System.out.print("\n Registro: "+vetorFazendas[i].registro);                        
                            System.out.print("\n Nome: "+vetorFazendas[i].nome);                        
                            System.out.print("\n Caixa: "+vetorFazendas[i].caixa);                        
                            System.out.print("\n Finalidade: "+vetorFazendas[i].finalidade);                        
                            System.out.print("\n Número de funcionários: "+vetorFazendas[i].numeroFuncionarios);
                            validador = 1;
                        }
                    }
                    if(validador==0){
                        System.out.println("\n Nenhum registro encontrado");
                    }
                break;

                case 4:
                    System.out.println("\n --- Empréstimo ---");
                   
                    float valor;
                    
                    System.out.print("\n Digite o registro da primeira fazenda: ");
                    entrada.skip("\n");
                    regisA = entrada.nextLine();
                    System.out.print("\n Digite o registro da segunda fazenda: ");
                    //entrada.skip("\n");
                    regisB = entrada.nextLine();
                    
                    for(i=0; i<3; i++){
                        if((vetorFazendas[i].registro).equals(regisA)){
                            a = i;
                        }else if((vetorFazendas[i].registro).equals(regisB)){
                            b = i;
                        }
                    }

                    System.out.print("\n Valor para transferência: ");
                    valor = entrada.nextFloat();
                    
                    vetorFazendas[a].caixa = vetorFazendas[a].caixa-valor;
                    vetorFazendas[b].caixa = vetorFazendas[b].caixa+valor;
                    
                    System.out.println("\n --- Saldos ---");
                    System.out.print("\n Fazenda "+(a+1)+": "+vetorFazendas[a].caixa);
                    System.out.print("\n Fazenda "+(b+1)+": "+vetorFazendas[b].caixa+"\n");                    
                break;

                case 5:
                    System.out.println("\n --- Trânsito de funcionários ---");
                    
                    int nmrFuncionarios=0;
                    
                    System.out.print("\n Digite o registro da primeira fazenda: ");
                    entrada.skip("\n");
                    regisA = entrada.nextLine();
                    System.out.print("\n Digite o registro da segunda fazenda: ");
                    //entrada.skip("\n");
                    regisB = entrada.nextLine();
                    
                    for(i=0; i<3; i++){
                        if((vetorFazendas[i].registro).equals(regisA)){
                            a = i;
                        }else if((vetorFazendas[i].registro).equals(regisB)){
                            b = i;
                        }
                    }

                    System.out.print("\n Número de funcionários em trânsito: ");
                    nmrFuncionarios = entrada.nextInt();
                    
                    vetorFazendas[a].numeroFuncionarios = vetorFazendas[a].numeroFuncionarios-nmrFuncionarios;
                    vetorFazendas[b].numeroFuncionarios = vetorFazendas[b].numeroFuncionarios+nmrFuncionarios;
                    
                    System.out.println("\n --- Número de funcionários ---");
                    System.out.print("\n Fazenda "+(a+1)+": "+vetorFazendas[a].numeroFuncionarios);
                    System.out.print("\n Fazenda "+(b+1)+": "+vetorFazendas[b].numeroFuncionarios+"\n");
                break;           

            }
        }
        
        
        
        
        
             
        
        
        
    }
    
}
